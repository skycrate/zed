import {
	names,
	keys, entries,
	map, filter
	apply, extend, concat, copy
} from "./lib/core/index.js";

export class Zobject {
	get [TYPE] {
		return this.constructor;
	}
	get [Symbol.species]() {
		return this.constructor;
	}
	constructor() {
		// I could put that "applier" code in here as default....
		// Something to think aboot...
	}
	super(Type, ...args) {
		inherit(this, Type, ...args);
	}
	define(props) {
		Object.defineProperties(this, props);
	}
	define_values(vals) {
		values(this, vals);
	}
	keys() {
		return keys(this);
	}
	values() {
		return Object.values(this);
	}
	entries() {
		return entries(this);
	}
	map(mapper) {
		return map(this, mapper);
	}
	filter(filterer) {
		return filter(this, filterer);
	}
	reduce(...args) {
		return entries(this).reduce(...args);
	}
	extend(...objects) {
		return extend(this, ...objects);
	}
	concat(...objects) {
		return concat(this, ...objects);
	}
	copy() {
		return copy(this);
	}
	apply(values) {
		return apply(this, values);
	}
	satisfies(type) {
		return satisfies(this[TYPE], type);
	}
}
const DEFAULT_PROTOTYPE = ZObject.prototype;
export const META_TYPES = names(
	'type', // The TYPE type. A type that describes a type...
	'template', // The TEMPLATE type. The type that defines how to build types.
	'descriptor', // The DESCRIPTOR type. Describes template property configuration.
	'value', // A descriptor which describes static values
	'variable',
	'accessor', // A descriptor which describes variables (get-set)
	'procedure', // The PROCEDURE type. A template for building function types...
	'param',
	'modifier', // The MODIFIER type. A procedure that builds DESCRIPTORS.
	'generator' // The GENERATOR type. A procedure that creates TEMPLATES.
);
const META_PROTOTYPE = {
	defines(instance) {
		return satisfies(instance[TYPE], this);
	}
};

export const assemble = {
	object: (properties = o(), prototype = o()) =>
		Object.create(concat(DEFAULT_PROTOTYPE, prototype), properties), //map(properties, conversion_map)),
	function: (getter, properties, prototype = o()) => {
		const obj = extend((...args) => func(...args), assemble.object(properties), prototype);
		const func = getter(obj);
		return obj;
	},
	constructor: (properties, prototype, applier) => {
		return function constructor(...args) {
			return extend(applier(assemble.object(properties, prototype), ...args), {constructor});
		};
	},
	class: (properties, prototype, applier, _BASE = ZObject) => {
		const constructor = prototype.constructor || x => x;
		delete prototype.constructor;
		const o = class extends _BASE {
			constructor(...args) {
				super(); // what aboot pasing arguments here?
				this.define(concat(...parents.map(parent => parent.properties), definition.properties));
				constructor.apply(this, args);
			}
		};
		extend(o.prototype, prototype);
		return o;
	}
};

export const construct = {
	model: (properties, prototype, _defaults = o()) =>
		assemble.constructor(
			properties, prototype,
			prototype.constructor ?
				(obj, ...args) =>
					prototype.constructor.apply(apply(obj, _defaults), args) || obj :
				(obj, defaults = o()) =>
					apply(obj, concat(_defaults, defaults))),
	procedure: (properties, prototype = o(), _defaults = o()) =>
		prototype.constructor ?
			(...args) =>
				assemble.function(obj => prototype.constructor.apply(obj, args), properties, prototype) :
			func =>
				assemble.function(obj => func.bind(obj), properties, prototype),
	class: (properties, prototype) => {
		
	},
	list: (properties, prototype) => {

	},
};


export const create = (properties, prototype) => assemble.object(assemble.values(properties), prototype);
export const constructor = (init, prototype) => (...args) => create(init(...args), prototype);
export const inherit = (_this, parent, ...args) => parent.prototype.constructor.apply(_this, args);
export const satisfies = (t, T) => t === T || t.parents.some(t => satisfies(t, T));

export const property = (value = null, enumerable = false, writable = false) => {
	return {value, enumerable, writable};
};
export const properties = (obj, props, enumerable = false, descriptor = value) =>
				define(obj, map(props, ([key, value]) => [key, descriptor(value, enumerable)]));

export const value = (value, enumerable = false) => property(value, enumerable);
export const values = (obj, props, enumerable = false) => properties(obj, props, enumerable);

export const variable = (value, enumerable = false) => property(value, enumerable, true);
export const variables = (obj, properties, enumerable = false) => properties(obj, props, enumerable, variable);

export const accessor = (get = obj => undefined, set = (obj, value) => undefined, enumerable = true, key = Symbol()) => {
	return {
		get() {
			let val = this[key];
			return defined(val) ? val : this[key] = get(this);
		},
		set(value) {
			return this[key] === value ? value : this[key] = set(this, value);
		},
		enumerable
	};
};
export const getter = (get = obj => undefined, enumerable = true) => {
	return {
		get() {
			return get(this);
		},
		enumerable
	}
};




// OK,maybe instead we'll be sticking 
export const prop = extend(type => {
	return accessor(
		obj => type.default_value,
		(obj, value) => type.validate(value) ? value :
							error(`Failed to set value '${value}' to ${obj}. Invalid Type.`),
		true
	);
}, {
	string: (value = "") => prop(type.string(value)),
	number: (value = 0) => prop(type.number(value)),
	int: (value = 0) => prop(type.int(value)),
	float: (value = 0.0) => prop(type.float(value)),
	func: (value = DO_NOTHING) => prop(type.function(value)),
});



export const MetaType = (type = Type, id = META_TYPES.type, prototype = o(), properties = o(), parents = []) => {
	return create({
		id,
		properties,
		parents,
		prototype,
		type,
	}, META_PROTOTYPE);
};
export const Type = (id, creator, prototype, properties = o(), defaults = o(), parents = [], statics = o()) => {
	let constructor = creator(
		concat(...parents.map(parent => parent.properties), properties),
		concat(...parents.map(parent => parent.prototype), prototype),
		defaults);
	return extend(constructor, statics, MetaType(
		constructor, id, prototype, properties, parents
	));
};
extend(Type, MetaType(
	Type,
	META_TYPES.type,
	META_PROTOTYPE,
	MetaType()
));
